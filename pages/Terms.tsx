import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Markdown from '../src/modules/components/Markdown';
import Typography from '../src/modules/components/Typography';
import AppAppBar from '../src/modules/views/AppAppBar';
import AppFooter from '../src/modules/views/AppFooter';
import withRoot from '../src/modules/withRoot';
import terms from '../src/modules/views/terms.md';

function Terms() {
  return (
    <React.Fragment>
      <AppAppBar />
      <Container>
        <Box mt={7} mb={12}>
          <Typography variant="h3" gutterBottom marked="center" align="center">
            Terms
          </Typography>
          <Markdown>{terms}</Markdown>
        </Box>
      </Container>
      <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Terms);

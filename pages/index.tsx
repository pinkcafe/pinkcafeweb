import Head from 'next/head'
import React from 'react';
import 'fontsource-roboto'
import ProductCategories from '../src/modules/views/ProductCategories';
import ProductSmokingHero from '../src/modules/views/ProductSmokingHero';
import AppFooter from '../src/modules/views/AppFooter';
import ProductHero from '../src/modules/views/ProductHero';
import ProductValues from '../src/modules/views/ProductValues';
import ProductHowItWorks from '../src/modules/views/ProductHowItWorks';
import ProductCTA from '../src/modules/views/ProductCTA';
import AppAppBar from '../src/modules/views/AppAppBar';
import withRoot from '../src/modules/withRoot';

function Index() {
  return (
    <React.Fragment>
        <Head>
        <title>Pink Cafe Tijuana | Pink Cafe | Espresso | Bikini Coffee Stands | Drive Thru</title>
        <meta name="keywords" content="tijuana coffee stand, tijuana espresso, bikini espresso, tijuana coffee shop, coffee, bikini coffee, lingerie espresso, drive thru espresso "></meta>
        </Head>
        <AppAppBar />
        <ProductHero />
        <ProductValues />
        <ProductCategories />
        <ProductHowItWorks />
        <ProductCTA />
        <ProductSmokingHero />
        <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Index);
